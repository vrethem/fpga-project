library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Address_generator is
	port (
		CLK, VGA_CLK, reset: in std_logic;
		song_select : in std_logic_vector(4 downto 0);
		refill_index : in std_logic_vector(4 downto 0);
		hcnt,vcnt   : in unsigned(9 downto 0);
      blank       : in std_logic; 
		
		push_sel : out unsigned(2 downto 0);			-- -> Sound_Buffer
		push 		: out std_logic;							-- -> Sound_Buffer
		up_lo_byte : out std_logic;						-- -> PIXEL_REG
		play_index : out std_logic_vector(4 downto 0);---> Sound_extractor
		address : out unsigned(19 downto 0);		   -- ->SRAM
		sram_ce,sram_oe,sram_we : out std_logic;		-- ->SRAM
      sram_lb,sram_ub         : out std_logic		-- ->SRAM
	);
end entity;

architecture rtl of Address_generator is
	signal pxl_address : unsigned(19 downto 0);
	signal aud_address : unsigned(19 downto 0);
	signal up_lo : std_logic;
	
	--signal AndersTMP : std_logic_vector(4 downto 0);
	
	component Sample_Controller is
	port (
		Enable : in std_logic;
		song_select : in std_logic_vector(4 downto 0);
		refill_index : in std_logic_vector(4 downto 0);
		CLK : in std_logic;
		reset : in std_logic;
		
		address_out : out unsigned(19 downto 0);
		push_sel : out unsigned (2 downto 0);
		play_idx : out std_logic_vector(4 downto 0); -- Which sound is playing
		push : out std_logic
	);
	end component;

begin 
process (reset, VGA_CLK)
	begin
	if reset = '0' then
		pxl_address <= (others => '0');
		up_lo <= '0';
	elsif rising_edge(VGA_CLK) then
		up_lo <= not up_lo;
	   if((hcnt > 640) and (vcnt > 479)) then 
			pxl_address <= (others => '0');
			up_lo <= '0';
	   elsif((up_lo = '1') and (blank = '1')) then
			pxl_address <= pxl_address + 1;
		end if;
	end if;
end process;
--AndersTMP(0) <= refill_index;
--AndersTMP(1) <= '0';
--AndersTMP(2) <= '0';
--AndersTMP(3) <= '0';
--AndersTMP(4) <= '0';

-- Reset  
Audio_controller : Sample_Controller
	port map (
		Enable => NOT blank,
		song_select => song_select, 
		refill_index => refill_index,
		CLK => CLK, 
		reset => reset,
		
		address_out => aud_address,
		push_sel => push_sel,
		play_idx => play_index,
		push  => push
	);
	
-- Input
	
-- Output
	up_lo_byte <= up_lo;
-- Mux for sram addresses
--	with blank select address <=
--		pxl_address when '1',
--		aud_address when '0';
	address <= pxl_address when blank = '1'
	else aud_address when blank = '0';
	
		
 sram_ce <= '0';
 sram_lb <= '0';
 sram_oe <= '0';
 sram_ub <= '0';
 sram_we <= '1';	
end rtl; 