library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM_control is
  Port ( clk,rstn   : in std_logic;
         hcnt,vcnt   : in unsigned(9 downto 0);
         blank       : in std_logic; -- same pipeline stage as hcnt,vcnt.
         -- RAM signals
         addr                    : out unsigned(19 downto 0);
         sram_ce,sram_oe,sram_we : out std_logic;
         sram_lb,sram_ub         : out std_logic;
			up_lo_byte  : out std_logic); -- '0' <=> read lo byte.);
end RAM_control;

architecture rtl of RAM_control is
signal sram_addr : unsigned (19 downto 0);
signal k : std_logic := '1';
begin

	sram_ce <='0';
	sram_oe <='0';
	sram_we <='1';
	sram_lb <='0';
	sram_ub <='0';

	process(clk, rstn, hcnt, vcnt, blank)
	begin
		if(rstn = '0') then
		sram_addr <= (others => '0');
		k <= '0';
		elsif(rising_edge(clk)) then
			k <= not k;
			if(vcnt > 479 and hcnt > 640) then
				sram_addr <= (others => '0');
				k <= '0';
			elsif(k = '1' and blank = '1') then
				sram_addr <= sram_addr + 1;
			end if;
		end if;
	end process;
	up_lo_byte <= k;
	addr <= sram_addr;
end rtl;
