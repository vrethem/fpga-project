library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adcdatcode is
	port(
		clk,rstn : in std_logic;
		sample : in signed (15 downto 0);
		adcdat : out std_logic
	);
end entity;

architecture arc of adcdatcode is
	signal cntr2 : unsigned(6 downto 0);
	signal samplebit : unsigned(3 downto 0); 
begin
	process(clk,rstn) 
	begin
		if(rstn = '0') then
			samplebit <= "1111";
			cntr2 <= "0000000";
		elsif(rising_edge(clk)) then
			if(cntr2 = 31) then 
				cntr2 <= "0000000";
				if samplebit = 0 then --When samplebitcounter reaches 16 go to next bit in sample
					samplebit <= "1111";
				else
					samplebit <= samplebit - 1;
				end if;
			else
				cntr2 <= cntr2 + 1;
			end if;
		end if;
	end process;
	
	adcdat <= sample(to_integer(samplebit));
end architecture;