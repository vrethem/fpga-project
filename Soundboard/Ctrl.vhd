Library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Ctrl is
Port(
	clk : in std_logic;
	rstn : in std_logic;
	mclk : out std_logic;
	bclk : out std_logic;
	men : out std_logic;
	SCCnt : out unsigned(1 downto 0);
	BitCnt : out std_logic;
	BitCnt2 : out std_logic;
	adclrc, daclrc : out std_logic;
	lrsel : out std_logic;
	hcnt : out unsigned(9 downto 0);
   vcnt : out unsigned(9 downto 0);
	blank : out std_logic
	);
end entity;

architecture rtl of Ctrl is
Signal cntr : unsigned(9 downto 0);
Signal	hcnt1 : unsigned(9 downto 0);
Signal   vcnt1 : unsigned(9 downto 0);

begin
process(clk, rstn)
begin
if rstn = '0' then
	cntr <= (others => '0');
	mclk <= '1';
	bclk <= '0';
	men <= '0';
	adclrc <= '1';
	daclrc <= '1';
	lrsel <= '0';
	SCCnt <= (others => '0');
	BitCnt <= (others => '0');
	hcnt1 <= (others => '0');
	vcnt1 <= (others => '0');
elsif rising_edge(clk) then
	cntr <= cntr + 1;
	if ((cntr mod 2) = 0)	then
		mclk <= not(cntr(1));
	end if;
	if ((cntr mod 8) = 0) then
		bclk <= cntr(3);
	end if;
	if ((cntr mod 4) = 3) then
		men <= '1';
	end if;
	if ((cntr mod 4) = 0) then
		men <= '0';
	end if;

		BitCnt <= cntr(9);
		BitCnt2 <= cntr(8);
		SCCnt <= cntr(3 downto 2);

	if (cntr < 512) then
		adclrc <= '1';
		daclrc <= '1';
		lrsel <= '0';
	else
		adclrc <= '0';
		daclrc <= '0';
		lrsel <= '1';
	end if;

	if(cntr(1) = '1') then -- dividera clk med 2
		if(hcnt1 < 797) then
			hcnt1 <= hcnt1 + 1;
		else
			hcnt1 <= "0000000000";
		end if; -- hcnti < 797
	
		if (hcnt1 = 797) then -- var 654
			if (vcnt1 < 524) then
				vcnt1 <= vcnt1 + 1;
			else
				vcnt1 <= "0000000000";
			end if;
		end if;
		if ((hcnt1 < 640) and (vcnt1 < 480)) then
			blank <= '1';
		else
			blank <= '0';
		end if;
	end if;
end if;
end process;

hcnt <= hcnt1;
vcnt <= vcnt1;
--SCCnt <= SCCnt1;
--BitCnt <= BitCnt1;
end architecture;