library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity Fifo is
	port (
		Din : in std_logic_vector(15 downto 0);-- Data in
		CLK : in std_logic;					-- 
		reset : in std_logic;				--
		Queue : in std_logic;				-- 
		Dequeue : in std_logic;				-- 
		Dout	: out signed(15 downto 0); -- Data out
		is_full : out std_logic
		);
end entity;


architecture rtl of Fifo is
		signal dat_arr : std_logic_vector(31 downto 0);
		signal s_dout : signed(15 downto 0);	--signal_dout
		signal size : unsigned(1 downto 0);	
		signal overload_error : std_logic; 	-- Not in use 
begin
process (reset, CLK)
	begin
	if reset = '0' then
		size <= "00";
		dat_arr <= (others => '0');
		s_dout <= (others => '0');
		overload_error <= '0';
	elsif rising_edge(CLK) then
	
		-- queue()
		if queue = '1' then
			if size = "00" then
				dat_arr(31 downto 16) <= Din;
				size <= "01";
			elsif size = "01" then
				dat_arr(15 downto 0) <= dat_arr(31 downto 16);
				dat_arr(31 downto 16) <= Din;
				size <= "10";
			else
			-- Om Ã¶verlagring sker 
				overload_error <= '1'; 
			end if;
		end if;		
		
		-- dequeue()
		if dequeue = '1' then
			if size = "01" then
				Dout <= signed(dat_arr(31 downto 16));
				size <= "00";
			elsif size = "10" then
				Dout <= signed(dat_arr(15 downto 0));
				size <= "01";
			else
				-- FÃ¶rsÃ¶ker komma Ã¥t tom Fifo buffer
				-- kommer att ske vi start av program 
				--s_dout <= (others => '0');
				overload_error <= '1';
			end if;
		end if;
				
	end if;
end process;

-- Input

-- Output
is_full <= size(1);	-- Buffer is full if size == 2 

end rtl; 