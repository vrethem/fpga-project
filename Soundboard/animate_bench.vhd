LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

-- entity declaration for your testbench.Dont declare any ports here
ENTITY animate_bench IS 
END animate_bench;

ARCHITECTURE behavior OF animate_bench IS
   -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT animate  --'test' is the name of the module needed to be tested.
--just copy and paste the input and output ports of your module as such. 
    PORT( 
         VGA_CLK, rstn: in std_logic;
			enable	: in std_logic;
			pix_in	: in unsigned(7 downto 0);
			hcnt, vcnt : in unsigned(9 downto 0);
			
			pix_out 	: out unsigned(7 downto 0);
			frameCntr: out unsigned(5 downto 0);
			idex_X 	: out unsigned(9 downto 0); 
			idex_Y 	: out unsigned(9 downto 0)
        );
    END COMPONENT;
   --declare inputs and initialize them
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   --declare outputs and initialize them
	
	signal pix_in : unsigned(7 downto 0);
	signal pix_out: unsigned(7 downto 0);
	signal frameCntr: unsigned(5 downto 0);
	signal hcnt : unsigned(9 downto 0);
	signal vcnt : unsigned(9 downto 0);
	signal idx_X,idx_Y : unsigned(9 downto 0);
	
   -- Clock period definitions
   constant clk_period : time := 1 ns;
BEGIN
    -- Instantiate the Unit Under Test (UUT)
   uut: animate PORT MAP (
			 VGA_CLK => clk,
          rstn => reset,
			 enable => '1',
			 pix_in => pix_in,
			 hcnt => hcnt,
			 vcnt => vcnt,
			 
			 pix_out => pix_out,
			 frameCntr => frameCntr,
			 idex_X => idx_X,
			 idex_Y => idx_Y
        );       

   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;
   -- Stimulus process
  stim_proc: process
   begin   
			reset <= '0';
		wait for 10 ns;
			reset <='1';	
			pix_in <= (others => '0');
			
		pix_in <= "00000000";
	for x in 0 to 120 loop 
		for i in 0 to 797 loop
			hcnt <= TO_UNSIGNED(i, 10);
			--pix_in <= pix_in + 1;
			for j in 0 to 525 loop
				wait for 1 ns;
				vcnt <= TO_UNSIGNED(j, 10);
				pix_in <= pix_in + 1;
				
			END LOOP;
		END LOOP;
	END LOOP;
  end process;

END;