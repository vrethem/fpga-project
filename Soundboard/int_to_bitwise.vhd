library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity int_to_bitwise is
	port (
		sel : in unsigned(2 downto 0);
		s0 : out std_logic;	-- Bitwise select
		s1 : out std_logic;	-- Bitwise select
		s2 : out std_logic;	-- Bitwise select
		s3 : out std_logic;	-- Bitwise select
		s4 : out std_logic	-- Bitwise select
		);
end entity;


architecture rtl of int_to_bitwise is
	signal sig_vec : std_logic_vector(4 downto 0);
begin
	
	s0 <= '1' when sel = 0
	else '0';
	
	s1 <= '1' when sel = 1
	else '0';
	
	s2 <= '1' when sel = 2
	else '0';
	
	s3 <= '1' when sel = 3
	else '0';
	
	s4 <= '1' when sel = 4
	else '0';
	-- Input
--	s0 <= NOT sel(0) AND NOT sel(1) AND NOT sel(0);--0
--	s1 <= NOT sel(0) AND NOT sel(1) AND sel(0); --1
--	s2 <= NOT sel(0) AND sel(1) AND NOT sel(0); --2
--	s3 <= NOT sel(0) AND sel(1) AND sel(0); --3
--	s4 <= sel(2) AND NOT sel(1) AND NOT sel(0);-- 4
end rtl; 