library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Channel_mod is
port(
	clk,rstn : in std_logic;
	DAC : in signed(15 downto 0);
	ADC : out signed(15 downto 0);
	men : in std_logic;
	sel : in std_logic;
	SCCnt : in unsigned(1 downto 0);
	BitCnt : in std_logic;
	adcdat : in std_logic;
	dacdat : out std_logic
);
end entity;

architecture channelarch of Channel_mod is
signal RXReg : signed(15 downto 0);
signal TXReg : signed(15 downto 0);
begin

rx:process(clk, rstn) begin
	if(rstn = '0') then
		RXreg <= "0000000000000000";
	elsif rising_edge(clk) then
		if (sel = '0' AND SCCnt = "01" AND men = '1' AND BitCnt = '0') then
			RXreg(15 downto 1) <= RXreg(14 downto 0);
			RXreg(0) <= adcdat;
		end if;
	end if;
end process;

ADC <= RXreg;

tx:process(clk, rstn) begin
	if (rstn = '0') then
		TXreg <= "0000000000000000";
		elsif rising_edge(clk) then
		if(sel = '0') then
			if (SCCnt = "11" AND men = '1') then
				TXreg <= TXreg(14 downto 0) & '0';
			end if;
		else
			TXreg <= DAC;
		end if;
	end if;
end process;
dacdat <= TXreg(15);

end architecture;