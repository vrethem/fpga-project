library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity animate is
	port 
	(
		VGA_CLK, rstn: in std_logic; -- Proborly donÂ´t need pipelining if connected to fpga_clk ... 
		enable	: in std_logic;
		pix_in	: in unsigned(7 downto 0);
		hcnt, vcnt : in unsigned(9 downto 0);
		
		pix_out 	: out unsigned(7 downto 0)
	);

end entity;

architecture rtl of animate is
-- Constants and type def
	
	constant start_X 		: integer := 300;
	constant start_Y		: integer := 200;
	constant mouth_width : integer := 80;
	constant mouth_height: integer := 30;
	constant black_pix 	: unsigned(7 downto 0) := "11111111"; -- = -1
	constant error_pix 	: unsigned(7 downto 0) := "10011100"; -- = -100
	constant test_pix 	: unsigned(7 downto 0) := "01000010"; -- = 66
	constant row_0			: unsigned(7 downto 0) := "00000000"; -- = 0
	constant empty_buff_pix:unsigned(7 downto 0):= "10000000"; -- = -512
	type row_array is array (0 to mouth_width * mouth_height +10) of unsigned(7 downto 0);

	-- Actual signal 
	signal pix_buffer : row_array;
	signal frameCounter : unsigned(5 downto 0); -- Kinda like a line counter 
	
	signal index_X : unsigned(9 downto 0); 
	signal index_Y : unsigned(9 downto 0);
	
	signal pipe_hcnt : unsigned(9 downto 0);	-- Pipe signal with one so that redirected pixel is one pixel ahead in buffer because of pipeline
	
	signal re_routed_pix : unsigned(7 downto 0);
	signal reRoute : std_logic;
	
begin

	process (VGA_CLK, rstn)
	begin
		if rstn = '0' then
			frameCounter <= (others => '0');
			pix_buffer <= (others => empty_buff_pix);
			re_routed_pix <= black_pix;
			index_X <= (others => '0');
			index_Y <= (others => '0');
			reRoute <= '0';	-- Select -> Mux( re_routed_pix,  pix_in 	 )		
			
		elsif (rising_edge(VGA_CLK)) then
			-------------------------------------
			-- If  (enabled)
			-------------------------------------
			if (enable = '1') then
				
				
				-------------------------
				-- INSIDE animation area
				-------------------------
				if ( hcnt >= start_X AND hcnt <= (start_X + mouth_width) AND
					  vcnt >= start_Y AND vcnt <= (start_Y + mouth_height + mouth_height) ) then
					
					
					index_X <= pipe_hcnt - start_X; -- Index_X always one pixel ahead, creating a pipeline 
					index_Y <= vcnt - start_Y;
					
				-----------------------------------------------------------------------------------------------------------		
					-- Animation moves UP 	
				-----------------------------------------------------------------------------------------------------------
					if ( frameCounter > mouth_height ) then 		-- When frameCounter above 15 the animation needs to go UPWARDS on screen 
						reRoute <= '1';
						-- Black pixel area, shrinks as frameCounter gets larger 
						if ( index_Y < ((start_Y + start_Y) - frameCounter )) then 
							re_routed_pix <= black_pix;
							
						-- Output saved pixels from pix_buffer
						else
							re_routed_pix <= pix_buffer( TO_INTEGER(index_X + ((index_Y - mouth_height) * mouth_width)) );
						end if;
						
						
				-----------------------------------------------------------------------------------------------------------						
					-- Animation move DOWN
				-----------------------------------------------------------------------------------------------------------
					else
			
					----------------------------------------------------------------
					------ 		Start new Animation 							------------- -- Save the animation frame to array pix_buffer
					---------------------------------------------------------------- -- frameCounter = 0 means starting position (frame 0)
						if ( frameCounter = 0 AND index_Y < mouth_height) then 
							pix_buffer( TO_INTEGER((hcnt - start_X) + (index_Y * mouth_width))) <= pix_in;
							reRoute <= '0';
							--re_routed_pix <= pix_in;
						
									
						-------------------------------------------
						------- INSIDE BLACK AREA 					---- -- If true this means we are inside the line that should move down with the frameCounter
						------------------------------------------- -- Output black pixel
						elsif ( index_Y < frameCounter ) then
							re_routed_pix <= black_pix;
							reRoute <= '1';
							
						-------------------------------------------
						---- GET FROM BUFFER AREA 
						-------------------------------------------
						elsif ( index_Y > frameCounter ) AND (index_Y < (mouth_height + frameCounter)) then 
							re_routed_pix <= pix_buffer(TO_INTEGER(index_X + ((index_Y - frameCounter) * mouth_width)));
							reRoute <= '1';
						
						-------------------------------------------
						-- DONÃ‚Â´T CHANGE PIX AREA
						-------------------------------------------
						else
							reRoute <= '0';
							--re_routed_pix <= pix_in;
							-- re_routed_pix <= error_pix; 
						end if;
						
					end if;
					
					--------------------------
					-- OUTSIDE animation area 
					--------------------------
					else 
						reRoute <= '0';
						index_X <= (others => '0');
						index_Y <= (others => '0');
		
						
					-- Add cntr for a specific pixel when ENABLED 	
					if (hcnt = start_X + mouth_width + 10) AND (vcnt = (start_Y + mouth_height + mouth_height + 10)) then 
						frameCounter <= frameCounter + 1;
						re_routed_pix <= pix_buffer(0);
					end if;
					if (frameCounter > (mouth_height + mouth_height)) then
						frameCounter <= (others => '0');
					end if;
					end if;
				
			-------------------------------------
			--  else if NOT ENABLE				---- -- Full reset when Enable signal goes low
			-------------------------------------
			else 
				frameCounter <= (others => '0');
				pix_buffer <= (others => empty_buff_pix); 
			end if;
		end if;
	end process;

-- Input
pipe_hcnt <= hcnt + 1;

-- Output
pix_out <= re_routed_pix when reRoute = '1' 
		else pix_in;
		
end rtl;
