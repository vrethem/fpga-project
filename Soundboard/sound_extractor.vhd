library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity sound_extractor is
  Port ( 
			CLK, reset, sample_clk : in std_logic; 		-- Sample rate (freq of sound)
			Din 	: in signed(15 downto 0);
			play_idx : in std_logic_vector(4 downto 0);
			
			pop 	: out std_logic;
			pipe_sel : out std_logic;
			Dout 	: out signed(15 downto 0);
			pop_sel : out unsigned(2 downto 0)	-- -> Sound_Buffer
       );
end sound_extractor;
 
architecture rtl of sound_extractor is
	type stateFSM is (IDLE, INIT, s0,s00,s000,s1,s11,s111,s2,s22,s222,s3,s33,s333,s4,s44,s444, CALCULATE );
	signal state, next_state: stateFSM;
	
	signal sum : signed(17 downto 0);		-- 2 bits larger than Dout/Din in case of overflow
	signal divider : unsigned(2 downto 0);	-- Divider to the sum
	signal DoutTmp : signed(26 downto 0);
	signal holdDout : signed (15 downto 0);
type lut is array (2 to 5) of signed(8 downto 0);
constant div_floater: lut :=(
5 => "000110100",
4 => "001000000",
3 => "001010001",
2 => "010000000");


begin
process (reset, CLK, SAMPLE_CLK)
begin
if reset = '0' then
--	state <= IDLE;
	pop <= '0';
	pipe_sel <= '0';
	Dout <= (others => '0');
	
elsif rising_edge(CLK) then	
  
	-- Here goes the state_machine 
	
	case state is 
	  
	  when IDLE =>
			if sample_clk = '1' then
				next_state <= INIT; 
				Dout <= holdDout;
			else	
				next_state <= IDLE;
			end if;
			
		when INIT => 
			next_state <= S0;
			divider <= "000";
			sum <= to_signed(0, 18);
-- S0
		when S0 =>
		if play_idx(0) = '1' then
			next_state <= S00;
			pop_sel <= "000";
			pipe_sel <= '1';
		else 
			next_state <= S1;
		end if;
		
		when S00 =>
			pop <= '1';
			next_state <= S000;
			
		when S000 => 
			sum <= sum + (Din(15) & Din(15) & Din);
			divider <= divider + 1;
			pop <= '0';
			pipe_sel <= '0';
			next_state <= S1;
-- S1			
		when S1 =>
		if play_idx(1) = '1' then
			next_state <= S11;
			pop_sel <= "001";
			pipe_sel <= '1';
		else 
			next_state <= S2;
		end if;
		
		when S11 =>
			pop <= '1';
			next_state <= S111;
			
		when S111 => 
			sum <= sum + (Din(15) & Din(15) & Din);
			divider <= divider + 1;
			pop <= '0';
			pipe_sel <= '0';
			next_state <= S2;

-- S2	
		when S2 =>
		if play_idx(2) = '1' then
			next_state <= S22;
			pop_sel <= "010";
			pipe_sel <= '1';
		else 
			next_state <= S3;
		end if;
		
		when S22 =>
			pop <= '1';
			next_state <= S222;
			
		when S222 => 
			sum <= sum + (Din(15) & Din(15) & Din);
			divider <= divider + 1;
			pop <= '0';
			pipe_sel <= '0';
			next_state <= S3;
-- S3
	when S3 =>
		if play_idx(3) = '1' then
			next_state <= S33;
			pop_sel <= "011";
			pipe_sel <= '1';
		else 
			next_state <= S4;
		end if;
		
		when S33 =>
			pop <= '1';
			next_state <= S333;
			
		when S333 => 
			sum <= sum + (Din(15) & Din(15) & Din);
			divider <= divider + 1;
			pop <= '0';
			pipe_sel <= '0';
			next_state <= S4;
-- S4		
	when S4 =>
		if play_idx(4) = '1' then
			next_state <= S44;
			pop_sel <= "100";
			pipe_sel <= '1';
		else 
			next_state <= CALCULATE;
		end if;
		
		when S44 =>
			pop <= '1';
			next_state <= S444;
			
		when S444 => 
			sum <= sum + (Din(15) & Din(15) & Din);
			divider <= divider + 1;
			pop <= '0';
			pipe_sel <= '0';
			next_state <= CALCULATE;	

		when CALCULATE =>
		  next_state <= IDLE;
			-- if divider > 0 then -- If no sound is playing  
				 
				 --holdDout <= resize(sum, 16);
				holdDout <= (sum(17) & sum(14 downto 0));
				-- holdDout <= sum(17 downto 2);
--				 case divider is
--					when "001" =>
--						Dout <= resize(sum, 16); 
--					when "010" =>
--						--DoutTmp <= sum * div_floater(2);
--						--Dout <= DoutTmp(24 downto 9);
--						Dout <= shift_left(resize(sum,16), 1);
--					when "011" =>
--					--	DoutTmp <= sum * div_floater(3);
--					--	Dout <= DoutTmp(24 downto 9);
--						Dout <= shift_left(resize(sum,16), 2) + shift_left(resize(sum,16), 1);
--					when "100" => 
----						DoutTmp <= sum * div_floater(4);
----						Dout <= DoutTmp(24 downto 9);
--						Dout <= shift_left(resize(sum,16), 2);
--					when "101" =>
----						DoutTmp <= sum * div_floater(5);
----						Dout <= DoutTmp(24 downto 9);
--						Dout <= shift_left(resize(sum,16), 3) + shift_left(resize(sum,16), 2);
--					when others =>
--						--Dout <= sum(17 downto 2);
--				 end case;
		
		when others =>
			next_state <= INIT;
			
		end case;
		
  end if;

end process;
 state <= next_state;
end rtl;