-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 12.1 Build 243 01/31/2013 Service Pack 1.33 SJ Full Version"
-- CREATED		"Tue Oct 17 11:21:03 2017"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY S_BUFFER IS 
	PORT
	(
		CLK :  IN  STD_LOGIC;
		reset :  IN  STD_LOGIC;
		dequeue :  IN  STD_LOGIC;
		queue :  IN  STD_LOGIC;
		Din :  IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
		sel :  IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
		Dout :  OUT  STD_LOGIC_VECTOR(15 DOWNTO 0);
		refill_index :  OUT  STD_LOGIC_VECTOR(4 DOWNTO 0)
	);
END S_BUFFER;

ARCHITECTURE bdf_type OF S_BUFFER IS 

COMPONENT mux4to1
	PORT(m1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 m2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 m3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 m4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 m5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 mout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT min_fifo
	PORT(CLK : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 Queue : IN STD_LOGIC;
		 Dequeue : IN STD_LOGIC;
		 Din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 is_full : OUT STD_LOGIC;
		 Dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT int_to_bitwise
	PORT(sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 s1 : OUT STD_LOGIC;
		 s2 : OUT STD_LOGIC;
		 s3 : OUT STD_LOGIC;
		 s4 : OUT STD_LOGIC;
		 s5 : OUT STD_LOGIC
	);
END COMPONENT;

SIGNAL	refill_index_ALTERA_SYNTHESIZED :  STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_5 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_6 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_7 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_8 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_25 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_26 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_27 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_15 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_16 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_17 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_18 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_19 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_20 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_28 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_29 :  STD_LOGIC;


BEGIN 



b2v_inst : mux4to1
PORT MAP(m1 => SYNTHESIZED_WIRE_0,
		 m2 => SYNTHESIZED_WIRE_1,
		 m3 => SYNTHESIZED_WIRE_2,
		 m4 => SYNTHESIZED_WIRE_3,
		 m5 => SYNTHESIZED_WIRE_4,
		 sel => sel,
		 mout => Dout);


b2v_inst0 : min_fifo
PORT MAP(CLK => CLK,
		 reset => reset,
		 Queue => SYNTHESIZED_WIRE_5,
		 Dequeue => SYNTHESIZED_WIRE_6,
		 Din => Din,
		 is_full => refill_index_ALTERA_SYNTHESIZED(0),
		 Dout => SYNTHESIZED_WIRE_0);


b2v_inst1 : min_fifo
PORT MAP(CLK => CLK,
		 reset => reset,
		 Queue => SYNTHESIZED_WIRE_7,
		 Dequeue => SYNTHESIZED_WIRE_8,
		 Din => Din,
		 is_full => refill_index_ALTERA_SYNTHESIZED(1),
		 Dout => SYNTHESIZED_WIRE_1);


SYNTHESIZED_WIRE_15 <= queue AND SYNTHESIZED_WIRE_25;


SYNTHESIZED_WIRE_16 <= SYNTHESIZED_WIRE_25 AND dequeue;


SYNTHESIZED_WIRE_17 <= queue AND SYNTHESIZED_WIRE_26;


SYNTHESIZED_WIRE_18 <= SYNTHESIZED_WIRE_26 AND dequeue;


SYNTHESIZED_WIRE_19 <= queue AND SYNTHESIZED_WIRE_27;


SYNTHESIZED_WIRE_20 <= SYNTHESIZED_WIRE_27 AND dequeue;


b2v_inst2 : min_fifo
PORT MAP(CLK => CLK,
		 reset => reset,
		 Queue => SYNTHESIZED_WIRE_15,
		 Dequeue => SYNTHESIZED_WIRE_16,
		 Din => Din,
		 is_full => refill_index_ALTERA_SYNTHESIZED(2),
		 Dout => SYNTHESIZED_WIRE_2);


b2v_inst3 : min_fifo
PORT MAP(CLK => CLK,
		 reset => reset,
		 Queue => SYNTHESIZED_WIRE_17,
		 Dequeue => SYNTHESIZED_WIRE_18,
		 Din => Din,
		 is_full => refill_index_ALTERA_SYNTHESIZED(3),
		 Dout => SYNTHESIZED_WIRE_3);


b2v_inst4 : min_fifo
PORT MAP(CLK => CLK,
		 reset => reset,
		 Queue => SYNTHESIZED_WIRE_19,
		 Dequeue => SYNTHESIZED_WIRE_20,
		 Din => Din,
		 is_full => refill_index_ALTERA_SYNTHESIZED(4),
		 Dout => SYNTHESIZED_WIRE_4);


b2v_inst5 : int_to_bitwise
PORT MAP(sel => sel,
		 s1 => SYNTHESIZED_WIRE_28,
		 s2 => SYNTHESIZED_WIRE_29,
		 s3 => SYNTHESIZED_WIRE_25,
		 s4 => SYNTHESIZED_WIRE_26,
		 s5 => SYNTHESIZED_WIRE_27);


SYNTHESIZED_WIRE_5 <= queue AND SYNTHESIZED_WIRE_28;


SYNTHESIZED_WIRE_6 <= SYNTHESIZED_WIRE_28 AND dequeue;


SYNTHESIZED_WIRE_7 <= queue AND SYNTHESIZED_WIRE_29;


SYNTHESIZED_WIRE_8 <= SYNTHESIZED_WIRE_29 AND dequeue;

refill_index <= refill_index_ALTERA_SYNTHESIZED;

END bdf_type;