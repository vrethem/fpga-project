library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity KEY_TEST is
	port (
		CLK : in std_logic;					-- 
		reset : in std_logic;				--
		
		song_sel : in std_logic_vector(4 downto 0);-- Data in
		vol_sel : in std_logic_vector(3 downto 0);-- Data in
		
		SONG_LEDS	: out std_logic_vector(4 downto 0); -- Data out
		VOL_LEDS	: out std_logic_vector(3 downto 0) -- Data out
		);
end entity;


architecture rtl of KEY_TEST is
	signal cntrQ :  unsigned(32 downto 0);
	signal cntr1 :  unsigned(32 downto 0);
	signal cntr2 :  unsigned(32 downto 0);
	signal cntr3 :  unsigned(32 downto 0);
	signal cntr4 :  unsigned(32 downto 0);
	
	signal song : std_logic_vector(4 downto 0);-- Data in
	signal vol : std_logic_vector(3 downto 0);-- Data in
begin
process(clk,reset) begin
	if (reset = '0') then
		cntrQ <= (others => '0');
		cntr1 <= (others => '0');
		cntr2 <= (others => '0');
		cntr3 <= (others => '0');
		cntr4 <= (others => '0');
		
		song <= "00000";
		vol <= "0000";
	elsif rising_edge(clk) then
		if(cntrQ > 25000000) then
			cntrQ <=(others => '0');
			song(0) <= '0';
		else
			cntrQ <= cntrQ + 1;
		end if;
		
		if(cntr1 > 25000000) then
			cntr1 <=(others => '0');
			song(1) <= '0';
		else
			cntr1 <= cntr1 + 1;
		end if;
		
		if(cntr2 > 25000000) then
			cntr2 <=(others => '0');
			song(2) <= '0';
		else
			cntr2 <= cntr2 + 1;
		end if;
		
		if(cntr3 > 25000000) then
			cntr3 <=(others => '0');
			song(3) <= '0';
		else
			cntr3 <= cntr3 + 1;
		end if;
		
		if(cntr4 > 25000000) then
			cntr4 <=(others => '0');
			song(4) <= '0';
		else
			cntr4 <= cntr4 + 1;
		end if;
		
		if song_sel(0) = '1' then
			song(0) <= '1';
			cntrQ <= (others => '0');
		elsif song_sel(1) = '1' then
			song(1) <= '1';
			cntr1 <= (others => '0');
		elsif song_sel(2) = '1' then
			song(2) <= '1';
			cntr2 <= (others => '0');
		elsif song_sel(3) = '1' then
			song(3) <= '1';
			cntr3 <= (others => '0');
		elsif song_sel(4) = '1' then
			song(4) <= '1';
			cntr4 <= (others => '0');
		end if;
			
		if vol_sel(0) = '1' then
			vol(0) <= '1';
		elsif vol_sel(1) = '1' then
			vol(1) <= '1';
		elsif vol_sel(2) = '1' then
			vol(2) <= '1';
		elsif vol_sel(3) = '1' then
			vol(3) <= '1';
		end if;
	end if;
end process;

-- Input

-- Output
sonG_LEDS <= song;
vol_LEDS <= vol;
end rtl; 