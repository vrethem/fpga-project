library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_gen is
  Port ( clk,rstn : in std_logic;
         pixrg :  in unsigned(7 downto 0);
         blank2 : in std_logic;
			vcnt : in unsigned(9 downto 0);
			hcnt : in unsigned(9 downto 0);
			play_index : in std_logic_vector(4 downto 0);
			volume : in unsigned(3 downto 0);
			balance : in unsigned(7 downto 0);
         vga_r :  out unsigned(7 downto 0);
         vga_g :  out unsigned(7 downto 0);
         vga_b :  out unsigned(7 downto 0);
         vga_blank : out std_logic;
         vga_sync  : out std_logic);
end vga_gen;

architecture behave of vga_gen is
begin
process(clk, rstn, pixrg, blank2)
begin

vga_blank <= blank2;

if (rstn = '0') then
vga_r <= "00000000";
vga_g <= "00000000";
vga_b <= "00000000";
vga_blank <= '0';
--volume <= "1010";
--balance <= "00001010";
--play_index <= "10010";
elsif (rising_edge(clk)) then
if(hcnt > 49 and hcnt < 69 and vcnt > 37 and vcnt < 57 and play_index(0) = '1')
	or ( hcnt > 49 and hcnt < 69 and vcnt > 67 and vcnt < 87 and play_index(1) = '1')
	or ( hcnt > 49 and hcnt < 69 and vcnt > 97 and vcnt < 117 and play_index(2) = '1')
	or ( hcnt > 49 and hcnt < 69 and vcnt > 127 and vcnt < 147 and play_index(3) = '1')
	or ( hcnt > 49 and hcnt < 69 and vcnt > 157 and vcnt < 177 and play_index(4) = '1')
	or (hcnt > 74 and hcnt < (74+(to_integer(unsigned(volume)))*20)  and vcnt > 428 and vcnt < 448)	
	or ((hcnt > (162 - ((to_integer(unsigned(balance(7 downto 4))))- (to_integer(unsigned(balance(3 downto 0))))) * 9) --438
	and hcnt < 184 - ((to_integer(unsigned(balance(7 downto 4))))- (to_integer(unsigned(balance(3 downto 0))))) * 9)
	and vcnt > 398 and vcnt < 418) then --408
	vga_r <= (others => '1');
	vga_g <= (others => '0'); --338 - 130
	vga_b <= (others => '1');
	elsif( hcnt > 334 and hcnt < 340 and vcnt > 127 and vcnt < 133) then
	vga_r <= (others => '1');
	vga_g <= (others => '0'); --338 - 130
	vga_b <= (others => '0');
	elsif (pixrg(7) = '1') then
	vga_r <= pixrg(6 downto 5) & pixrg(6 downto 5) & pixrg(6 downto 5) & pixrg(6 downto 5);
	vga_g <= pixrg(4 downto 2) & pixrg(4 downto 2) & pixrg(4 downto 3);
	vga_b <= pixrg(1 downto 0) & pixrg(1 downto 0) & pixrg(1 downto 0) & pixrg(1 downto 0);
	else
	vga_r <= pixrg(6 downto 0) & '0';
	vga_g <= pixrg(6 downto 0) & '0';
	vga_b <= pixrg(6 downto 0) & '0';
	end if;
end if;

end process;
vga_sync <= '0';
end behave;