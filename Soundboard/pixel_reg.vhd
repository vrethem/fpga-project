library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity pixel_reg is
  Port ( CLK, up_lo_byte : in std_logic; -- '0' <=> read lo byte.
         pixrg : out unsigned(7 downto 0);
         -- RAM signals
			Din : in unsigned(15 downto 0)
       );
end pixel_reg;
 
architecture rtl of pixel_reg is
 signal higher_byte : unsigned(7 downto 0);
 signal lower_byte : unsigned(7 downto 0);
begin
	process(CLK, up_lo_byte, higher_byte, lower_byte)
	begin
	if rising_edge(clk) then
		if (up_lo_byte = '1') then
			pixrg <= higher_byte;
		else
			pixrg <= lower_byte;
		end if;
	end if;
	end process;
	
	higher_byte <= Din(15 downto 8);
	lower_byte <= Din(7 downto 0);
end rtl;
