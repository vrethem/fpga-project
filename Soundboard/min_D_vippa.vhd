library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity min_D_vippa is
	port (
		clk : in std_logic;
		dat : in unsigned(2 downto 0);
		dout : out unsigned(2 downto 0)
		);
end entity;


architecture rtl of min_D_vippa is
	signal s_2 : std_logic_vector(2 downto 0);	-- Bitwise select
begin
process(CLK)
begin
if rising_edge(CLK) then
	dout <= dat;
end if;
end process;
end rtl; 