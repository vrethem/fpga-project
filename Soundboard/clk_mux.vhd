library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_mux is
port(
	clk,mux  : in  std_logic;
	auxclk,ramclk,rstnaux,rstnram : out std_logic
);
end entity;

architecture clk_mux_arc of clk_mux is
begin
	auxclk <= clk when mux = '1' else '0';
	ramclk <= clk when mux = '0' else '0';
	
	rstnaux <= '1' when mux = '1' else '0';
	rstnram <= '1' when mux = '0' else '0';
end architecture;