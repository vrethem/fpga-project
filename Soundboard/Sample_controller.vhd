library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Sample_Controller is
	port (
		Enable : in std_logic;
		song_select : in std_logic_vector(4 downto 0);
		refill_index : in std_logic_vector(4 downto 0);
		CLK : in std_logic;
		reset : in std_logic;
		
		address_out : out unsigned(19 downto 0);
		push_sel : out unsigned(2 downto 0);
		play_idx : out std_logic_vector(4 downto 0); -- Which sound is playing
		push : out std_logic
	);
end entity;

architecture rtl of Sample_Controller is
	type ptr_array is array (4 downto 0) of unsigned(19 downto 0);
	signal ptr : ptr_array;
	signal play_index : std_logic_vector(4 downto 0) := "00000";
	
	type stateFSM is (IDLE, C0, C1, C2, C3, REFILL_STATE, PUSH_STATE, DONE_STATE);
	signal state, next_state: stateFSM;
	signal sel : unsigned(2 downto 0); 
	signal WE : std_logic := '0';
	
	constant pic0 : integer := 307200/2;
	constant audio1 : integer := pic0 + 38822;--(77644/2);
	constant audio2 : integer := audio1 + 52063; --(104126/2);
	constant audio3 : integer := audio2 + 60898;--(121796/2);
	constant audio4 : integer := audio3 + 87088;--(174176/2);
	constant audio5 : integer := audio4 + 115965;--(231930/2);
	
begin
process (reset, CLK)
	begin
	if reset = '0' then
		sel <= "000";
		ptr	<= (others => to_unsigned( 0, 20)); 
		play_index <= (others => '0');
		WE <= '0';
		
	elsif rising_edge(CLK) then
--SOUND 1 
		if song_select(0) = '1' then 
				play_index(0)	<= '1';
				ptr(0)	<= to_unsigned(audio1 + 1, 20); -- PEKARE MÅSTE SÄTTAS HÄR 
			elsif (ptr(0) > audio2 ) then		-- ADDRESSER TILL SRAM MÅSTE SÄTTAS HÄR 
				play_index(0) <= '0';
			end if;
--SOUND 2		
			if song_select(1) = '1' then 
				play_index(1)	<= '1';
				ptr(1)	<= to_unsigned(pic0 + 1, 20);
			elsif ( ptr(1) > audio1 ) then
				play_index(1) <= '0';
			end if;	
--SOUND 3 			
			if song_select(2) = '1' then 
				play_index(2)	<= '1';
				ptr(2)	<= to_unsigned(audio2 + 1, 20);
			elsif ( ptr(2) > audio3 ) then
				play_index(2) <= '0';
			end if;
--SOUND 4			
			if song_select(3) = '1' then 
				play_index(3)	<= '1';
				ptr(3)	<= to_unsigned(audio3 + 1, 20);
			elsif ( ptr(3) > audio4) then
				play_index(3) <= '0';
			end if;
--SOUND 5		
			if song_select(4) = '1' then 
				play_index(4)	<= '1';
				ptr(4)	<= to_unsigned(audio4 + 1, 20);
			elsif ( ptr(4) > audio5 ) then
				play_index(4) <= '0';
			end if;
		
-------------------------------------------------------------------		
		if Enable = '1' then 	
-- This is the STATE_MACHINE 		
		case state is 
			when IDLE => 
				if refill_index(0) = '1' then
					 next_state <= REFILL_STATE;
					 sel <= "000";
				else
					next_state <= C0;
				end if;
			
			when C0 	=>
				if refill_index(1) = '1' then
					next_state <= REFILL_STATE;
					sel <= "001";
				else
					next_state <= C1;
				end if;
			
			when C1 => 
				if refill_index(2) = '1' then
					next_state <= REFILL_STATE;
					sel <= "010";
				else
					next_state <= C2;
				end if;
			
			when C2 => 
				if refill_index(3) = '1' then
					next_state <= REFILL_STATE;
					sel <= "011";
				else
					next_state <= C3;
				end if;
			
			when C3 => 
				if refill_index(4) = '1' then
					next_state <= REFILL_STATE;
					sel <= "100"; 
				else 
					next_state <= IDLE;
				end if;
			
			when REFILL_STATE =>
				push_sel <= sel;
				next_state <= PUSH_STATE;
				
			when PUSH_STATE => 
				WE <= '1';						-- push from SRAM to soundbuffer (sends EN to soundbuffer)
				next_state <= DONE_STATE;
				
			when DONE_STATE => 
				WE <= '0'; 
				next_state <= IDLE;
				ptr(to_integer(sel)) <= ptr(to_integer(sel)) + 1;	-- Add +1 to address pointer
			
			when others =>
				next_state <= IDLE;
				WE <= '0'; 
				
		end case;
	end if;
	
	end if;
end process;

state <= next_state;

-- Input

-- Output
address_out <= ptr(to_integer(sel)); -- Address is ptr.at(push_sel)
push <= WE;
play_idx <= play_index;
end rtl; 
