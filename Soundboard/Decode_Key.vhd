library ieee;
use ieee.std_logic_1164.all; --KANSKE FEL BIBBLA
use ieee.numeric_std.all;

entity Decode_Key is
	port( rstn, clk, PS2_CLK, PS2_DAT : in std_logic;
	volume_key : out unsigned(3 downto 0);
	song_select : out std_logic_vector(4 downto 0);
	audio_select : out std_logic );
	end entity;

architecture rtl of Decode_Key is
 -- Declare signal
Signal PS2_CLK2 		: std_logic;
Signal PS2_CLK2_old 	: std_logic;
Signal PS2_DAT2 		: std_logic;
Signal detected_fall : std_logic;
signal shiftreg: std_logic_vector(21 downto 0);
signal volume_key1 : unsigned(3 downto 0);
signal song_select1 : std_logic_vector(4 downto 0);
signal audio_select1 : std_logic;

begin
p1 : process(clk) begin
	if rising_edge(clk) then
	PS2_DAT2 <= PS2_DAT;
	PS2_CLK2 <= PS2_CLK;
	PS2_CLK2_old <= PS2_CLK2;
	end if;
end process;
detected_fall <= (NOT PS2_CLK2 AND PS2_CLK2_old);-- FIX!!!
 
p2 : process(clk, rstn) begin
	if rstn = '0' then
		song_select1 <= (others => '0');
		volume_key1 <= (others => '0');
		audio_select1 <= '0';
		shiftreg <= (others => '0');
	elsif rising_edge(clk) then
		if detected_fall = '1' then
			shiftreg(21 downto 0) <= PS2_DAT2 & shiftreg(21 downto 1);
			end if;
		if shiftreg(19 downto 12) = "11110000"  then
			case shiftreg(8 downto 1) is		
		when "00010101" => song_select1 <= "00001";
		when "00011101" => song_select1 <= "00010";
		when "00100100" => song_select1 <= "00100";
		when "00101101" => song_select1 <= "01000";
		when "00101100" => song_select1 <= "10000";
		
		when "00111100" => volume_key1 <= "0001";
		when "00110001" => volume_key1 <= "0010";
		when "00101010" => volume_key1 <= "0100";
		when "00110011" => volume_key1 <= "1000";
		
		when "00111010" => audio_select1 <= not(audio_select1);
		when others => song_select1 <= "00000"; volume_key1 <= "0000";
		end case;
		
	shiftreg <= (others => '0');
	else
		song_select1 <= (others => '0');
		volume_key1 <= (others => '0');
	end if;
	end if;
	end process;
	volume_key <= volume_key1;
	audio_select <= audio_select1;
	song_select <= song_select1;
	
end architecture;