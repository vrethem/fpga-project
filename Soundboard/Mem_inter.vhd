-- Copyright (C) 1991-2012 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 12.1 Build 243 01/31/2013 Service Pack 1.33 SJ Full Version"
-- CREATED		"Tue Oct 17 10:01:48 2017"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;
LIBRARY work;

ENTITY Mem_inter IS 
	PORT
	(
		CLK :  IN  STD_LOGIC;
		reset :  IN  STD_LOGIC;
		blank :  IN  STD_LOGIC;
		sample_clk :  IN  STD_LOGIC;
		VGA_CLK :  IN  STD_LOGIC;
		hcnt :  IN  UNSIGNED(9 DOWNTO 0);
		Song_select :  IN  STD_LOGIC_VECTOR(4 DOWNTO 0);
		SRAM_DATA :  IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
		vcnt :  IN  UNSIGNED(9 DOWNTO 0);
		SRAM_CE :  OUT  STD_LOGIC;
		SRAM_OE :  OUT  STD_LOGIC;
		SRAM_WE :  OUT  STD_LOGIC;
		SRAM_LB :  OUT  STD_LOGIC;
		SRAM_UB :  OUT  STD_LOGIC;
		SRAM_address :  OUT  UNSIGNED(19 DOWNTO 0);
		VGA_CONTROLLER :  OUT  UNSIGNED(7 DOWNTO 0);
		VOLUME_BALANCE :  OUT  SIGNED(15 DOWNTO 0)
	);
END Mem_inter;

ARCHITECTURE bdf_type OF Mem_inter IS 

ATTRIBUTE black_box : BOOLEAN;
ATTRIBUTE noopt : BOOLEAN;

COMPONENT busmux_0
	PORT(sel : IN STD_LOGIC;
		 dataa : IN integer range 0 to 4; 
		 datab : IN integer range 0 to 4; 
		 result : OUT integer range 0 to 4 );
END COMPONENT;
ATTRIBUTE black_box OF busmux_0: COMPONENT IS true;
ATTRIBUTE noopt OF busmux_0: COMPONENT IS true;

COMPONENT address_generator
	PORT(CLK : IN STD_LOGIC;
		 VGA_CLK : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 blank : IN STD_LOGIC;
		 hcnt : IN UNSIGNED(9 DOWNTO 0);
		 refill_index : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		 song_select : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		 vcnt : IN UNSIGNED(9 DOWNTO 0);
		 push : OUT STD_LOGIC;
		 up_lo_byte : OUT STD_LOGIC;
		 sram_ce : OUT STD_LOGIC;
		 sram_oe : OUT STD_LOGIC;
		 sram_we : OUT STD_LOGIC;
		 sram_lb : OUT STD_LOGIC;
		 sram_ub : OUT STD_LOGIC;
		 address : OUT UNSIGNED(19 DOWNTO 0);
		 play_index : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
		 push_index : OUT integer range 0 to 4
	);
END COMPONENT;

COMPONENT s_buffer
	PORT(CLK : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 queue : IN STD_LOGIC;
		 dequeue : IN STD_LOGIC;
		 Din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 sel : IN integer range 4 downto 0;
		 Dout : OUT SIGNED(15 DOWNTO 0);
		 refill_index : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
	);
END COMPONENT;

COMPONENT sound_extractor
	PORT(CLK : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 sample_clk : IN STD_LOGIC;
		 Din : IN SIGNED(15 DOWNTO 0);
		 play_idx : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
		 RE : OUT STD_LOGIC;
		 pipe_sel : OUT STD_LOGIC;
		 Dout : OUT SIGNED(15 DOWNTO 0);
		 pop_index : OUT integer range 0 to 4
	);
END COMPONENT;

COMPONENT pixel_reg
	PORT(CLK : IN STD_LOGIC;
		 up_lo_byte : IN STD_LOGIC;
		 Din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 pixrg : OUT UNSIGNED(7 DOWNTO 0)
	);
END COMPONENT;

SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_3 :  integer range 0 TO 4;
SIGNAL	SYNTHESIZED_WIRE_4 :  SIGNED(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_5 :  STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_6 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_7 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_8 :  integer range 0 TO 4;
SIGNAL	SYNTHESIZED_WIRE_9 :  integer range 0 TO 4;


BEGIN 



b2v_Address_gen : address_generator
PORT MAP(CLK => CLK,
		 VGA_CLK => VGA_CLK,
		 reset => reset,
		 blank => blank,
		 hcnt => hcnt,
		 refill_index => SYNTHESIZED_WIRE_0,
		 song_select => Song_select,
		 vcnt => vcnt,
		 push => SYNTHESIZED_WIRE_1,
		 up_lo_byte => SYNTHESIZED_WIRE_6,
		 sram_ce => SRAM_CE,
		 sram_oe => SRAM_OE,
		 sram_we => SRAM_WE,
		 sram_lb => SRAM_LB,
		 sram_ub => SRAM_UB,
		 address => SRAM_address,
		 play_index => SYNTHESIZED_WIRE_5,
		 push_index => SYNTHESIZED_WIRE_8);


b2v_inst : s_buffer
PORT MAP(CLK => CLK,
		 reset => reset,
		 dequeue => SYNTHESIZED_WIRE_2,
		 queue => SYNTHESIZED_WIRE_1,
		 Din => SRAM_DATA,
		 sel => SYNTHESIZED_WIRE_3,
		 Dout => SYNTHESIZED_WIRE_4,
		 refill_index => SYNTHESIZED_WIRE_0);


b2v_inst1 : sound_extractor
PORT MAP(CLK => CLK,
		 reset => reset,
		 sample_clk => sample_clk,
		 Din => SYNTHESIZED_WIRE_4,
		 play_idx => SYNTHESIZED_WIRE_5,
		 RE => SYNTHESIZED_WIRE_2,
		 pipe_sel => SYNTHESIZED_WIRE_7,
		 Dout => VOLUME_BALANCE,
		 pop_index => SYNTHESIZED_WIRE_9);


b2v_inst4 : pixel_reg
PORT MAP(CLK => VGA_CLK,
		 up_lo_byte => SYNTHESIZED_WIRE_6,
		 Din => SRAM_DATA,
		 pixrg => VGA_CONTROLLER);


b2v_MUX1 : busmux_0
PORT MAP(sel => SYNTHESIZED_WIRE_7,
		 dataa => SYNTHESIZED_WIRE_8,
		 datab => SYNTHESIZED_WIRE_9,
		 result => SYNTHESIZED_WIRE_3);


END bdf_type;