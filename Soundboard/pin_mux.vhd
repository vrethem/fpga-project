library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pin_mux is
port(
	mux  : in  std_logic;
	mclki,bclki,adclrci,daclrci,dacdati : in std_logic;
	mclki2,bclki2,adclrci2,daclrci2,dacdati2 : in std_logic;
	mclku,bclku,adclrcu,daclrcu,dacdatu : out std_logic
);
end entity;

architecture pin_mux_arc of pin_mux is
begin
	mclku <= mclki when mux = '0' else mclki2;
	bclku <= bclki when mux = '0' else bclki2;
	adclrcu <= adclrci when mux = '0' else adclrci2;
	daclrcu <= daclrci when mux = '0' else daclrci2;
	dacdatu <= dacdati when mux = '0' else dacdati2;
end architecture;