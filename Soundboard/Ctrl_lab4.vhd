library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Ctrl_lab4 is
port(
	clk,rstn  : in  std_logic;
	mclk : out std_logic;
	bclk : out std_logic;
	men : out std_logic;
	SCCnt : out unsigned(1 downto 0);
	BitCnt : out std_logic;
	adclrc, daclrc : out std_logic;
	lrsel : out std_logic
);
end entity;

architecture ctrllab4arch of Ctrl_lab4 is
signal cntr :  unsigned(9 downto 0);
begin

process(clk,rstn) begin
	if (rstn = '0') then
		cntr <= "0000000000";
	elsif rising_edge(clk) then
		cntr <= cntr + 1;
	end if;
end process;
		adclrc <= cntr(9);
		daclrc <= cntr(9);
		lrsel <= NOT cntr(9);
		mclk <= NOT cntr(1);
		men <= cntr(1) AND cntr(0);
		bclk <= cntr(3);
		SCCnt <= cntr(3 downto 2);
		BitCnt <= cntr(8);
END architecture;