library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--constant N : integer := 4; -- (N + 1) == nbr of sounds/Fifo's
entity sound_buffer is	
	port (
		Din : in signed(15 downto 0);				-- Data in
		RE : in std_logic;							-- Read Enable
		WE	: in std_logic;							-- Write Enable 
		sel : in integer range 0 to 4;			-- Decimal song select
		reset : in std_logic;						--
		CLK : in std_logic;							--
		
		refill_index : out std_logic_vector(4 downto 0);	-- Which fifo needs refill
		Dout : out signed(15 downto 0)
		);
end entity;

architecture rtl of sound_buffer is
	signal refill_idx : std_logic_vector(4 downto 0);
	signal s : std_logic_vector(4 downto 0);	-- Bitwise select
	signal s_2: unsigned(2 downto 0);
	
	type fifo_arr is array (4 downto 0) of signed(15 downto 0);
	signal dout_arr : fifo_arr;
	
	component FIFO
		port (
			Din : in signed(15 downto 0);		-- Data in
			CLK : in std_logic;					-- 
			reset : in std_logic;				--
			queue : in std_logic;					-- 
			dequeue : in std_logic;				-- 
			Dout	: out signed(15 downto 0); 	-- Data out
			is_full : out std_logic
		);
	end component;

	signal tempx : signed(15 downto 0);
	signal tempy, quex, dequex : std_logic;
begin	
process (reset, CLK)
	begin
		if reset = '0' then
			--refill_idx <= "00000";	
			--s <= "00000";
			--s_2 <= to_unsigned(0, 3);
		--elsif rising_edge(CLK) then	
		end if;
end process;
	
--	F1: FIFO port map ( Din => , CLK, reset, s(0) AND WE, s(0) AND RE, dout_arr(0), refill_idx(0) );
--	F2: FIFO port map ( Din, CLK, reset, s(1) AND WE, s(1) AND RE, dout_arr(1), refill_idx(1) );
--	F3: FIFO port map ( Din, CLK, reset, s(2) AND WE, s(2) AND RE, dout_arr(2), refill_idx(2) );
--	F4: FIFO port map ( Din, CLK, reset, s(3) AND WE, s(3) AND RE, dout_arr(3), refill_idx(3) );
--	F5: FIFO port map ( Din, CLK, reset, s(4) AND WE, s(4) AND RE, dout_arr(4), refill_idx(4) );
--	
	GEN_FIFO:
	for I in 0 to 4 generate 
		tempx <= Dout_arr(I);
		tempy <= refill_idx(I);
		quex <= s(I) AND WE;
		dequex <= s(I) AND RE;
		FIFO_REG : FIFO port map (Din => Din, CLK => CLK, reset => reset, queue => quex, dequeue => dequex, Dout => tempx, is_full => tempy);
	end generate GEN_FIFO;
	
-- Input
s_2 <= to_unsigned(sel, 4);
s(4) <= s_2(2) AND NOT s_2(1) AND NOT s_2(0);
s(3) <= NOT s_2(0) AND s_2(1) AND s_2(0);
s(2) <= NOT s_2(0) AND s_2(1) AND NOT s_2(0);
s(1) <= NOT s_2(0) AND NOT s_2(1) AND s_2(0);
s(0) <= NOT s_2(0) AND NOT s_2(1) AND NOT s_2(0);

-- Output mux
with sel select Dout <=
		dout_arr(0) when 0,
		dout_arr(1) when 1,
		dout_arr(2) when 2,
		dout_arr(3) when 3,
		dout_arr(4) when 4;


refill_index <= refill_idx;

--process (sel)
--begin	
--	s <= ( others => '0');
--		if sel = 4 then
--			s(4) <= '1';
--			Dout <= dout_arr(4);
--		elsif (sel = 3) then
--			s(3) <= '1';
--			Dout <= dout_arr(3);
--		elsif sel = 2 then
--			s(2) <= '1';
--			Dout <= dout_arr(2);
--		elsif sel = 1 then
--			s(1) <= '1';
--			Dout <= dout_arr(1);
--		else 
--			s(0) <= '1';
--			Dout <= dout_arr(0);
--	end if;
--end process;


end rtl; 