library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sound_adder is
	port(
	sample1,sample2 : in signed(15 downto 0);
	sampleout : out signed(15 downto 0)
	);
end entity;

architecture arc3 of sound_adder is
	signal tmp : signed(16 downto 0);
begin
	
	tmp <= (resize(sample1, 17) + resize(sample2,17));
	sampleout <= tmp(16 downto 1);
end architecture;