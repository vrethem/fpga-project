library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Volume is
port(
volume_key : in unsigned(3 downto 0);
clk : in std_logic;
rstn : in std_logic;
audio_dataL : in signed(15 downto 0);
audio_dataR : in signed(15 downto 0);
volume_dataL : out signed(15 downto 0);
volume_dataR : out signed(15 downto 0);
volume : out unsigned(3 downto 0);
balance : out unsigned(7 downto 0));
end entity;

architecture vol of Volume is
--signal counter : unsigned(1 downto 0);
Signal vol_v : unsigned(3 downto 0);
Signal left_bal : unsigned(3 downto 0); 
Signal right_bal : unsigned(3 downto 0);
Signal left_reg : signed(33 downto 0);
Signal right_reg : signed(33 downto 0);

type lut is array (0 to 10) of signed(8 downto 0);
constant bitcount: lut :=(
10 => "011111111",
9 => "011000000",  
8 => "010000000",
7 => "001011001",
6 => "001000000",
5 => "000101110",
4 => "000100001",
3 => "000010111",
2 => "000001111",
1 => "000001011",
0 => "000000000");

begin

process(clk, rstn)
begin
if (rstn = '0') then
vol_v <= "0000";
left_bal <= "1010";
right_bal <= "1010";
--counter <= "00";
left_reg <= (others => '0');
right_reg <= (others => '0');
elsif (rising_edge(clk)) then
	if((volume_key > "0000")) then
		
	if (volume_key = "0001" and vol_v < 10) then
		vol_v <= vol_v + "0001";
		
	elsif (volume_key = "0010" and vol_v > 0) then
		vol_v <= vol_v - "0001";
		
	elsif (volume_key = "0100" and left_bal = 10 and right_bal > 0) then
		right_bal <= right_bal - "0001";	
	elsif (volume_key = "1000" and right_bal < 10) then 
		right_bal <= right_bal + "0001";
	elsif (volume_key = "1000" and right_bal = 10 and left_bal > 0) then
		left_bal <= left_bal - "0001";
	elsif (volume_key = "0100" and left_bal < 10) then
		left_bal <= left_bal + "0001";
	
	end if;
end if;

--if left_bal = 0 or vol_v = 0 then
--		left_reg <= (others => '0');
--else
		left_reg <= bitcount(to_integer(unsigned(vol_v))) * bitcount(to_integer(unsigned(left_bal))) * audio_dataL;
--end if;

--if right_bal = 0 or vol_v = 0  then
--		right_reg <= (others => '0');
--else
		right_reg <= bitcount(to_integer(unsigned(vol_v))) * bitcount(to_integer(unsigned(right_bal))) * audio_dataR; 
--end if;

end if;
end process;
		volume_dataL <= left_reg(33 downto 18);
		volume_dataR <= right_reg(33 downto 18); 
		volume <= vol_v;
		balance <= left_bal & right_bal;
end architecture;