library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity mux4to1 is
	port (
		m1 : in signed(15 downto 0);-- Data in
		m2 : in signed(15 downto 0);-- Data in
		m3 : in signed(15 downto 0);-- Data in
		m4 : in signed(15 downto 0);-- Data in
		m5 : in signed(15 downto 0);-- Data in
		sel : in unsigned(2 downto 0);
		
		mout	: out signed(15 downto 0) -- Data out
		);
end entity;


architecture rtl of mux4to1 is
begin
	mout <= m2 when sel = "001"
		else m3 when sel = "010"
		else m4 when sel = "011"
		else m5 when sel = "100"
		else m1;
end rtl; 