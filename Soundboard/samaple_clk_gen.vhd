library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sample_clk_gen is
port(
	clk,rstn  : in  std_logic;
	sample_clk : out std_logic
);
end entity;

architecture ctrlarch of sample_clk_gen is
signal cntr :  unsigned(9 downto 0);
begin

process(clk,rstn) begin
	if (rstn = '0') then
		cntr <= "00000000000";
	elsif rising_edge(clk) then
		if(cntr = 1023) then
			cntr <= "0000000000";
			sample_clk <= '1';
		else
			sample_clk <= '0';
			cntr <= cntr + 1;
		end if;
	end if;
end process;
end architecture;