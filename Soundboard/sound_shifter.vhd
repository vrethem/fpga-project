library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sound_shifter is
	port(
	clk,rstn : in std_logic;
	sample : in signed(15 downto 0);
	sampleout : out signed(15 downto 0)
	);
end entity;

architecture arc5 of sound_shifter is
	signal sampletmp : signed(15 downto 0);
	signal cntr : unsigned(5 downto 0);
begin
	process(clk,rstn) begin
		if(rstn = '0') then
			cntr <= (others => '0');
		elsif(rising_edge(clk)) then
			if(cntr = 31) then
				cntr <= (others => '0');
				sampletmp <= sample;
			else
				cntr <= cntr + 1;
			end if;
		end if;
	end process;
	sampleout <= sampletmp;
end architecture;