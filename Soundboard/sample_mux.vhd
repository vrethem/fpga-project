library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sample_mux is
port(
	mux  : in  std_logic;
	LAux,LRam,RAux,RRam : in signed(15 downto 0);
	L,R : out signed(15 downto 0)
);
end entity;

architecture sample_mux_arc of sample_mux is
begin
	L <= Laux when mux = '1' else LRam;
	R <= Raux when mux = '1' else RRam;
end architecture;